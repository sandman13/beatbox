/* Java source file for MusicServer class */
import java.io.*;
import java.net.*;
import java.util.*;

public class MusicServer {
	ArrayList<ObjectOutputStream> clientOutputStreams;

	public static void main(String[] args) {
		new MusicServer().go();
	}

	/* Inner class ClientHandler */
	public class ClientHandler implements Runnable {
		ObjectInputStream in;
		Socket clientSocket;

		/* One arg constructor */
		public ClientHandler(Socket socket) {
			try {
				clientSocket = socket;
				in = new ObjectInputStream(clientSocket.getInputStream());
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}

		public void run() {
			Object objTwo = null;
			Object objOne = null;
			try {
				while ((objOne = in.readObject()) != null) {
					objTwo = in.readObject();

					System.out.println("read two objects");
					tellEveryone(objOne, objTwo);
				}
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}
	}

	public void go() {
		clientOutputStreams = new ArrayList<ObjectOutputStream>();
		try {
			ServerSocket serverSock = new ServerSocket(4242);
            System.out.print("* Running on http://0.0.0.0:4242/ ");
            System.out.println("(Press CTRL+C to quit)");
			while(true) {
				Socket clientSocket = serverSock.accept();
				ObjectOutputStream out = new ObjectOutputStream(
						clientSocket.getOutputStream());
				clientOutputStreams.add(out);

				Thread t = new Thread(new ClientHandler(clientSocket));
				t.start();

				System.out.println("Got a connection!");
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		}
	}

	public void tellEveryone(Object one, Object two) {
		Iterator it = clientOutputStreams.iterator();
		while(it.hasNext()) {
			try {
				ObjectOutputStream out = (ObjectOutputStream) it.next();
				out.writeObject(one);
				out.writeObject(two);
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}
	}
}
