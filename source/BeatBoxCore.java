/* Java source file for BeatBoxCore class */
import java.awt.*;
import javax.swing.*;
import java.io.*;
import javax.sound.midi.*;
import java.util.*;
import java.awt.event.*;
import java.net.*;
import javax.swing.event.*;
import javax.swing.UIManager;

public class BeatBoxCore {
	int nextNum;
	ArrayList<JCheckBox> checkboxList;
	String userName;

	Vector<String> listVector = new Vector<String>();
	JFrame theFrame;
	JPanel mainPanel;
	JTextField userMessage;
	JList<String> incomingList;
	ObjectOutputStream out;
	ObjectInputStream in;
	HashMap<String, boolean[]> otherSeqsMap = new HashMap<String, boolean[]>();

	Sequencer sequencer;
	Sequence sequence;
	Sequence mySequence = null;
	Track track;

	String[] instrumentNames = {
        "Bass Drum", "Closed Hi-Hat", "Open Hi-Hat",
		"Acoustic Snare", "Crash Cymbal", "Hand Clap",
        "High Tom", "Hi Bongo", "Maracas", "Whistle",
        "Low Conga", "Cowbell", "Vibraslap",
        "Low-mid Tom", "High Agogo", "Open Hi Conga" };

	int[] instruments = { 35, 42, 46, 38, 49, 39, 50, 60, 70, 72, 64, 56, 58,
        47, 67, 63 };

	public static void main (String[] args) {
		/* args[0] is our user ID/screen name */
		if(args.length != 1) {
            System.out.println("Usage: java <classname> <username>");
		} else {
            new BeatBoxCore().startUp(args[0]);
		}
	}

	public void startUp(String name) {
		userName = name;
		// open connection to the server
		try {
			Socket sock = new Socket("127.0.0.1", 4242);
			out = new ObjectOutputStream(sock.getOutputStream());
			in = new ObjectInputStream(sock.getInputStream());
			Thread remote = new Thread(new RemoteReader());
			remote.start();
		} catch(Exception ex) {
			System.out.println("Couldn't connect - you'll have to play alone.");
		}
		setUpMidi();
        try{
            buildGUI();
        } catch(Exception ex) {
            System.out.println("Class not found!");
        }
	}

	public void buildGUI()
         throws ClassNotFoundException, InstantiationException,
                IllegalAccessException, UnsupportedLookAndFeelException {
        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		theFrame = new JFrame("Cyber BeatBox");
		BorderLayout layout = new BorderLayout();
		JPanel background = new JPanel(layout);
		background.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		theFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		checkboxList = new ArrayList<JCheckBox>();

		Box buttonBox = new Box(BoxLayout.Y_AXIS);
		JButton start = new JButton("Start");
		start.addActionListener(new MyStartListener());
		buttonBox.add(start);

		JButton stop = new JButton("Stop");
		stop.addActionListener(new MyStopListener());
		buttonBox.add(stop);

		JButton upTempo = new JButton("Tempo Up");
		upTempo.addActionListener(new MyUpTempoListener());
		buttonBox.add(upTempo);

		JButton downTempo = new JButton("Tempo Down");
		downTempo.addActionListener(new  MyDownTempoListener());
		buttonBox.add(downTempo);

		JButton sendIt = new JButton("Send It!");
		sendIt.addActionListener(new MySendListener());
		buttonBox.add(sendIt);

		userMessage = new JTextField();
		buttonBox.add(userMessage);

		incomingList = new JList<String>();
		incomingList.addListSelectionListener(new MyListSelectionListener());
		incomingList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		JScrollPane theList = new JScrollPane(incomingList);
		buttonBox.add(theList);
		incomingList.setListData(listVector); // no data to start with

		Box nameBox = new Box(BoxLayout.Y_AXIS);
		for (int i = 0; i < 16; i++) {
			nameBox.add(new Label(instrumentNames[i]));
		}

		background.add(BorderLayout.EAST, buttonBox);
		background.add(BorderLayout.WEST, nameBox);

		theFrame.getContentPane().add(background);
		GridLayout grid = new GridLayout(16, 16);
		grid.setVgap(1);
		grid.setHgap(2);
		mainPanel = new JPanel(grid);
		background.add(BorderLayout.CENTER, mainPanel);

		for (int  i = 0; i < 256; i++) {
			JCheckBox c = new JCheckBox();
			c.setSelected(false);
			checkboxList.add(c);
			mainPanel.add(c);
		}

		theFrame.setBounds(50, 50, 300, 300);
		theFrame.pack();
		theFrame.setVisible(true);
	}

	public void setUpMidi() {
		try {
			sequencer = MidiSystem.getSequencer();
			sequencer.open();
			sequence = new Sequence(Sequence.PPQ, 4);
			track = sequence.createTrack();
			sequencer.setTempoInBPM(120);
		} catch(Exception ex) {
			ex.printStackTrace();
		}
	}

	public void buildTrackAndStart() {
		/* This will hold the instruments for each */
		ArrayList<Integer> trackList = null;
		sequence.deleteTrack(track);
		track = sequence.createTrack();

		for (int i = 0; i < 16; i++) {
			trackList = new ArrayList<Integer>();

			for (int j = 0; j < 16; j++) {
				JCheckBox jc = checkboxList.get(j + (16*i));
				if (jc.isSelected()) {
					int key = instruments[i];
					trackList.add(new Integer(key));
				} else {
					/* because this slot should be empty in the track */
					trackList.add(null);
				}
			}
			makeTracks(trackList);
		}

		/* The line below - so that we always go to full 16 beats */
		track.add(makeEvent(192, 9, 1, 0, 15));
		try {
			sequencer.setSequence(sequence);
			sequencer.setLoopCount(Sequencer.LOOP_CONTINUOUSLY);
			sequencer.start();
			sequencer.setTempoInBPM(120);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	public class MyStartListener implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			buildTrackAndStart();
		}
	}

	public class MyStopListener implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			sequencer.stop();
		}
	}

	public class MyUpTempoListener implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			float tempoFactor = sequencer.getTempoFactor();
			sequencer.setTempoFactor((float) (tempoFactor * 1.03));
		}
	}

	public class MyDownTempoListener implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			float tempoFactor = sequencer.getTempoFactor();
			sequencer.setTempoFactor((float) (tempoFactor * 0.97));
		}
	}

	public class MySendListener implements ActionListener {
	/* We serialized two objects (the String message and the beat pattern)
     * and write those two objects to the socket output stream(to the server) */
		public void actionPerformed(ActionEvent event) {
			// make an arraylist of just the STATE of the checkboxes
			boolean[] checkboxState = new boolean[256];
			for (int i = 0; i < 256; i++) {
				JCheckBox check = checkboxList.get(i);
				if (check.isSelected()) {
					checkboxState[i] = true;
				}
			}
			String messageToSend = null;
			try {
				out.writeObject(userName + nextNum++ + ": " +
					userMessage.getText());
				out.writeObject(checkboxState);
			} catch(Exception ex) {
				System.out.println("Sorry, could not send it to the server.");
			}
			userMessage.setText("");
		}
	}

	public class MyListSelectionListener implements ListSelectionListener {
	/* ListSelectionListener that tells us when the user made a selection on
     * the list of messages.When the user selects a message, we IMMEDIATELY
     * load the associated beat patter (it's in the HashMap called otherSeqsMap)
     * and start playing it. There's some if tests because of little quirky
     * things about getting ListSelectionEvents
     */
		public void valueChanged(ListSelectionEvent le) {
			if (!le.getValueIsAdjusting()) {
				String selected = incomingList.getSelectedValue();
				if (selected != null) {
				// now go to the map, and change th sequence
					boolean[] selectedState = otherSeqsMap.get(selected);
					changeSequence(selectedState);
					sequencer.stop();
					buildTrackAndStart();
				}
			}
		}
	}

	public class RemoteReader implements Runnable {
	/* This is the thread job -- read in data from the server. In this
	* code, 'data' will always be two serialized objects the String
	* message and the beat pattern (an ArrayList of checkbox state
	* values)
	* When a message comes in, we read (deserialized) the two objects (the
	* message and the ArrayList of Boolean checkbox state values) and add
	* it to the JList component. Adding to a JList is a two-step thing: we
	* keep a Vector of the lists data (Vector is an old-fashioned
	* ArrayList), and then tell the JList to use that Vector as it's source
	* for what to display in the list
	*/
		boolean[] checkboxState = null;
		Object obj = null;
		public void run() {
			try {
				while((obj=in.readObject()) != null) {
					System.out.println("got an object from server");
					System.out.println(obj.getClass());
					String nameToShow = (String) obj;
					checkboxState = (boolean[]) in.readObject();
					otherSeqsMap.put(nameToShow, checkboxState);
					listVector.add(nameToShow);
					incomingList.setListData(listVector);
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}

	public class MyPlayMineListener implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			if (mySequence != null) {
				sequence = mySequence; // restore to my orginal
			}
		}
	}

	/* This method is called when the user selects something from the list.
	* We IMMEDIATELY change the pattern to the one they selected.
	*/
	public void changeSequence(boolean[] checkboxState) {
		for (int i = 0; i < 256; i++) {
			JCheckBox check = checkboxList.get(i);
			if (checkboxState[i]) {
				check.setSelected(true);
			} else {
				check.setSelected(false);
			}
		}
	}

	public void makeTracks(ArrayList<Integer> list) {
		Iterator<Integer> it = list.iterator();
		for (int i = 0; i < 16; i++) {
			Integer num = it.next();
			if (num != null) {
				int numKey = num.intValue();
				track.add(makeEvent(144, 9, numKey, 100, i));
				track.add(makeEvent(128, 9, numKey, 100, i + 1));
			}
		}
	}

	public MidiEvent makeEvent(int command, int channel, int note,
                               int velocity, int duration) {
		MidiEvent event = null;
		try {
			ShortMessage a = new ShortMessage();
			a.setMessage(command, channel, note, velocity);
			event = new MidiEvent(a, duration);
		} catch(Exception ex) {
			ex.printStackTrace();
		}
		return event;
	}
}
