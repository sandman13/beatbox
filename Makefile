CC=javac
CLASSDIR=classes
SOURCE=source/*.java

all: $(SOURCE)
	@mkdir -p $(CLASSDIR)
	@echo "Compiling..." 
	$(CC) $(SOURCE) -d $(CLASSDIR)
	@echo "Done!"
	@echo "Change to $(CLASSDIR) and run the following on different tabs"
	@echo ""
	@echo "	'$$ java MusicServer'"
	@echo "	'$$ java BeatBoxCore <username>'"

clean:
	@echo "Removing class files from $(CLASSDIR)..."
	$(RM) $(CLASSDIR)/*
	@echo "Done!"
