# BeatBox
A simple BeatBox application written using Java Swing components, Java MIDI
libraries

## Running
```
    $ git clone https://sandman13@bitbucket.org/sandman13/beatbox.git
    $ cd beatbox
    $ make
    $ cd classes
    $ java MusicServer # On one tab
    $ java BeatBoxCore # On another tab
```
